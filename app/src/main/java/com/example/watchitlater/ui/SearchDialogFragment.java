package com.example.watchitlater.ui;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.watchitlater.R;
import com.example.watchitlater.retrofit.ContentRetro;
import com.example.watchitlater.retrofit.JsonOmdbApi;
import com.example.watchitlater.room.actor.Actor;
import com.example.watchitlater.room.actor.ActorViewModele;
import com.example.watchitlater.room.content.Content;
import com.example.watchitlater.room.content.ContentViewModele;
import com.example.watchitlater.room.content_actor.ContentActor;
import com.example.watchitlater.room.content_actor.ContentActorViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@SuppressWarnings("WeakerAccess")
public class SearchDialogFragment extends DialogFragment {

    private ContentViewModele contentViewModele;
    private ActorViewModele actorViewModele;
    private ContentActorViewModel contentActorViewModel;

    private JsonOmdbApi jsonOmdbApi;

    private Content content;
    private String[] actors;

    private ImageView imageView;
    private TextView searchResult;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        // ViewModele
        contentViewModele =  MainActivity.getContentViewModele();
        actorViewModele = MainActivity.getActorViewModele();
        contentActorViewModel = MainActivity.getContentActorViewModele();

        // Retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.omdbapi.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonOmdbApi = retrofit.create(JsonOmdbApi.class);

        // Layout
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        @SuppressLint("InflateParams")
        View view = inflater.inflate(R.layout.dialog_search, null);

        // Layout elements
        final EditText search = view.findViewById(R.id.search);
        ImageButton searchButton = view.findViewById(R.id.searchButton);
        imageView = view.findViewById(R.id.poster);
        searchResult = view.findViewById(R.id.searchResult);

        // Launch API search
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getContent(search.getText().toString());
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.adb_title_search)
                .setView(view)
                .setPositiveButton(R.string.adb_btn_positive_add, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Add on database
                        if (content != null) {
                            new InsertAsyncTask().execute();
                        }
                        else {
                            Toasty.error(Objects.requireNonNull(getContext()), R.string.toast_error_add).show();
                        }
                    }
                })
                .setNegativeButton(R.string.adb_btn_negative_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }

    /**
     * Insert in database
     */
    @SuppressLint("StaticFieldLeak")
    private class InsertAsyncTask extends AsyncTask<Void, Void, Void> {

        private final ArrayList<Integer> actorsId = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Insert Content
            contentViewModele.insert(content);

            // Insert Actors
            for (String s : actors) {
                actorViewModele.insert(new Actor(s));
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {

            // getContentID
            int contentID = contentViewModele.getContentId(content.getTitle());

            // getActorsID
            for (String s : actors) {
                actorsId.add(actorViewModele.getActorId(s));
            }

            // Insert ContentActor
            for (Integer actorID : actorsId) {
                contentActorViewModel.insert(new ContentActor(contentID, actorID));
            }

            return null;
        }
    }

    /**
     * Search on API
     * @param search String
     */
    private void getContent(String search) {

        imageView.setImageDrawable(null);
        searchResult.setText(R.string.retro_in_search);

        Call<ContentRetro> call = jsonOmdbApi.getContent(search, getString(R.string.apiKey));

        call.enqueue(new Callback<ContentRetro>() {
            @SuppressWarnings("NullableProblems")
            @Override
            public void onResponse(Call<ContentRetro> call, Response<ContentRetro> response) {
                if (!response.isSuccessful()) {
                    Toasty.error(Objects.requireNonNull(getContext()), "Erreur "+response.code()).show();
                }
                else if (Objects.requireNonNull(response.body()).getTitle() != null) {

                    // response of retrofit
                    ContentRetro contentRetro = response.body();

                    // display response of retrofit
                    String data = getString(R.string.label_title)+" : "+contentRetro.getTitle()
                            +"\n\n"
                            +getString(R.string.label_released)+" : "+contentRetro.getReleased()
                            +"\n\n"
                            +getString(R.string.label_plot)
                            +"\n"+contentRetro.getPlot()
                            +"\n\n"
                            +getString(R.string.label_type)+" : "+contentRetro.getType();
                    searchResult.setText(data);

                    // display imageView
                    Picasso.get()
                            .load(contentRetro.getPoster())
                            //.placeholder(R.drawable.user_placeholder)
                            //.error(R.drawable.user_placeholder_error)
                            .into(imageView);

                    // new content
                    content = new Content(contentRetro.getTitle(),
                            contentRetro.getReleased(),
                            contentRetro.getRuntime(),
                            contentRetro.getPlot(),
                            contentRetro.getAwards(),
                            contentRetro.getPoster(),
                            contentRetro.getType());
                    content.setTotalSeasons(contentRetro.getTotalSeasons());

                    // actors String Array
                    actors = contentRetro.getActors().split(", ");
                }
                else {
                    searchResult.setText(R.string.retro_no_result);
                }
            }

            @SuppressWarnings("NullableProblems")
            @Override
            public void onFailure(Call<ContentRetro> call, Throwable t) {
                Toasty.error(Objects.requireNonNull(getContext()), Objects.requireNonNull(t.getMessage())).show();
                searchResult.setText(t.getMessage());
            }
        });
    }
}
