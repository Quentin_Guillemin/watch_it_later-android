package com.example.watchitlater.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.watchitlater.R;
import com.example.watchitlater.room.actor.Actor;
import com.example.watchitlater.room.actor.ActorViewModele;
import com.example.watchitlater.room.content.Content;
import com.example.watchitlater.room.content.ContentViewModele;
import com.example.watchitlater.room.content_actor.ContentActor;
import com.example.watchitlater.room.content_actor.ContentActorViewModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class ContentInfoActivity extends AppCompatActivity {

    private ContentViewModele contentViewModele;
    private ActorViewModele actorViewModele;
    private ContentActorViewModel contentActorViewModel;

    private List<Actor> deleteActorList;
    private Content deleteContent;

    private int contentID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_info);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Layout elements
        final TextView title = findViewById(R.id.title);
        final TextView released = findViewById(R.id.released);
        final TextView runtime = findViewById(R.id.runtime);
        final TextView actors = findViewById(R.id.actors);
        final TextView plot = findViewById(R.id.plot);
        final TextView awards = findViewById(R.id.awards);
        final ImageView poster = findViewById(R.id.poster);
        final TextView totalSeasons = findViewById(R.id.totalSeasons);

        // ViewModele
        contentViewModele = MainActivity.getContentViewModele();
        actorViewModele = MainActivity.getActorViewModele();
        contentActorViewModel = MainActivity.getContentActorViewModele();

        // Intent
        Intent intent = getIntent();
        contentID = intent.getIntExtra("contentID", 0);

        contentViewModele.getContent(contentID).observe(this, new Observer<Content>() {
            @Override
            public void onChanged(Content content) {
                if (content != null) {
                    deleteContent = content;

                    // ImageView
                    Picasso.get()
                            .load(content.getPoster())
                            //.placeholder(R.drawable.user_placeholder)
                            //.error(R.drawable.user_placeholder_error)
                            .into(poster);

                    // TextView
                    title.setText(content.getTitle());
                    released.setText(content.getReleased());
                    runtime.setText(content.getRuntime());
                    plot.setText(content.getPlot());
                    awards.setText(content.getAwards());

                    if (content.getTotalSeasons() != null) {
                        totalSeasons.setText(content.getTotalSeasons());
                    }
                    else {
                        TextView label_totalSeasons = findViewById(R.id.label_totalSeasons);
                        label_totalSeasons.setVisibility(View.INVISIBLE);
                        totalSeasons.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });

        contentActorViewModel.getAllActorsOfContent(contentID).observe(this, new Observer<List<Actor>>() {
            @Override
            public void onChanged(List<Actor> actorList) {
                deleteActorList = actorList;

                StringBuilder actorsString = new StringBuilder();
                int i = 1;
                for (Actor actor : actorList) {
                    actorsString.append(" - ");
                    actorsString.append(actor.getName());
                    if (i < actorList.size())
                        actorsString.append("\n");
                    i++;
                }
                actors.setText(actorsString);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_remove) {
            // AlertDialog confirmation remove
            AlertDialog.Builder adb = new AlertDialog.Builder(this);
            adb.setMessage(getText(R.string.adb_text_remove));
            adb.setPositiveButton(getText(R.string.adb_btn_positive_default), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    new DeleteAsyncTask().execute();
                    Toasty.success(getApplicationContext(), R.string.toast_success_delete).show();
                    finish();
                }
            });
            adb.setNegativeButton(getText(R.string.adb_btn_negative_default), null);
            adb.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("StaticFieldLeak")
    private class DeleteAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            List<ContentActor> deleteContentActor = contentActorViewModel.getContentActorOfContent(contentID);
            int nbContentsOfActor = contentActorViewModel.getNbContentsOfActor(contentID);

            // Delete ContentActor
            for (ContentActor contentActor : deleteContentActor) {
                contentActorViewModel.delete(contentActor);
            }

            // Delete Actor
            for (Actor actor : deleteActorList) {
                if (nbContentsOfActor == 1)
                    actorViewModele.delete(actor);
            }

            // Delete Content
            contentViewModele.delete(deleteContent);
            return null;
        }
    }
}
