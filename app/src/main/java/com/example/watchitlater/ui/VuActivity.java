package com.example.watchitlater.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.watchitlater.ContentAdapter;
import com.example.watchitlater.R;
import com.example.watchitlater.room.content.Content;
import com.example.watchitlater.room.content.ContentViewModele;

import java.util.List;
import java.util.Objects;

public class VuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_view);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        // ViewModele
        ContentViewModele contentViewModele = MainActivity.getContentViewModele();

        // RecyclerView
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        final ContentAdapter adapter = new ContentAdapter();
        recyclerView.setAdapter(adapter);

        contentViewModele.getContentsVue().observe(this, new Observer<List<Content>>() {
            @Override
            public void onChanged(List<Content> contents) {
                adapter.setContents(contents);
            }
        });

        adapter.setOnItemClickListener(new ContentAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Content content) {
                Intent intent = new Intent(getApplicationContext(), ContentInfoActivity.class);
                intent.putExtra("contentID", content.getId());
                startActivity(intent);
            }
        });
    }
}
