package com.example.watchitlater.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.watchitlater.ActorAdapter;
import com.example.watchitlater.R;
import com.example.watchitlater.room.actor.Actor;
import com.example.watchitlater.room.actor.ActorViewModele;

import java.util.List;
import java.util.Objects;

public class ActorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_view);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        // ViewModele
        ActorViewModele actorViewModele = MainActivity.getActorViewModele();

        // RecyclerView
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        final ActorAdapter adapter = new ActorAdapter();
        recyclerView.setAdapter(adapter);

        actorViewModele.getAllActors().observe(this, new Observer<List<Actor>>() {
            @Override
            public void onChanged(List<Actor> actors) {
                adapter.setActors(actors);
            }
        });

        adapter.setOnItemClickListener(new ActorAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Actor actor) {
                Intent intent = new Intent(getApplicationContext(), ActorInfoActivity.class);
                intent.putExtra("actorID", actor.getId());
                startActivity(intent);
            }
        });
    }
}
