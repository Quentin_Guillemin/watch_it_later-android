package com.example.watchitlater.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.watchitlater.ContentAdapter2;
import com.example.watchitlater.R;
import com.example.watchitlater.room.actor.Actor;
import com.example.watchitlater.room.actor.ActorViewModele;
import com.example.watchitlater.room.content.Content;
import com.example.watchitlater.room.content_actor.ContentActorViewModel;

import java.util.List;
import java.util.Objects;

public class ActorInfoActivity extends AppCompatActivity {

    private boolean hasActor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_view);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        // ViewModele
        ActorViewModele actorViewModele = MainActivity.getActorViewModele();
        ContentActorViewModel contentActorViewModel = MainActivity.getContentActorViewModele();

        // Intent
        Intent intent = getIntent();
        int actorID = intent.getIntExtra("actorID", 0);

        hasActor = false;

        if (actorID != 0) {
            actorViewModele.getActor(actorID).observe(this, new Observer<Actor>() {
                @Override
                public void onChanged(Actor actor) {
                    if (actor != null) {
                        Objects.requireNonNull(getSupportActionBar()).setTitle(actor.getName());
                        hasActor = true;
                    }
                }
            });
        }

        // RecyclerView
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        final ContentAdapter2 adapter = new ContentAdapter2();
        recyclerView.setAdapter(adapter);

        contentActorViewModel.getAllContentsOfActor(actorID).observe(this, new Observer<List<Content>>() {
            @Override
            public void onChanged(List<Content> contents) {
                if (hasActor) {
                    adapter.setContents(contents);
                }
            }
        });

        adapter.setOnItemClickListener(new ContentAdapter2.OnItemClickListener() {
            @Override
            public void onItemClick(Content content) {
                Intent intent = new Intent(getApplicationContext(), ContentInfoActivity.class);
                intent.putExtra("contentID", content.getId());
                startActivity(intent);
            }
        });
    }
}
