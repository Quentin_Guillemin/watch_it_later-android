package com.example.watchitlater.ui;

import android.content.Intent;
import android.os.Bundle;

import com.example.watchitlater.ConnexionInternet;
import com.example.watchitlater.ContentAdapter;
import com.example.watchitlater.R;
import com.example.watchitlater.room.actor.ActorViewModele;
import com.example.watchitlater.room.content.Content;
import com.example.watchitlater.room.content.ContentViewModele;
import com.example.watchitlater.room.content_actor.ContentActorViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class MainActivity extends AppCompatActivity {

    // Paramètres
    private long backPress;
    private Toast backToast;

    // ViewModele
    private static ContentViewModele CONTENT_VIEW_MODELE;
    private static ActorViewModele ACTOR_VIEW_MODELE;
    private static ContentActorViewModel CONTENT_ACTOR_VIEW_MODELE;

    // Initialisation de la vue "MainActivity"
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // FAB
        final FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnexionInternet.isConnectedInternet(MainActivity.this)) {
                    SearchDialogFragment searchDialog = new SearchDialogFragment();
                    searchDialog.show(getSupportFragmentManager(), "search dialog");
                }
                else {
                    Toasty.error(getApplicationContext(), R.string.toast_error_internet).show();
                }
            }
        });

        // ViewModele
        CONTENT_VIEW_MODELE = new ViewModelProvider(this).get(ContentViewModele.class);
        ACTOR_VIEW_MODELE = new ViewModelProvider(this).get(ActorViewModele.class);
        CONTENT_ACTOR_VIEW_MODELE = new ViewModelProvider(this).get(ContentActorViewModel.class);

        // RecyclerView
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        final ContentAdapter adapter = new ContentAdapter();
        recyclerView.setAdapter(adapter);

        CONTENT_VIEW_MODELE.getContentsNonVue().observe(this, new Observer<List<Content>>() {
            @Override
            public void onChanged(List<Content> contents) {
                adapter.setContents(contents);
                fab.show();
            }
        });

        adapter.setOnItemClickListener(new ContentAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Content content) {
                Intent intent = new Intent(getApplicationContext(), ContentInfoActivity.class);
                intent.putExtra("contentID", content.getId());
                startActivity(intent);
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0)
                    fab.hide();
                else if (dy < 0)
                    fab.show();
            }
        });
    }

    public static ContentViewModele getContentViewModele() {
        return CONTENT_VIEW_MODELE;
    }

    public static ActorViewModele getActorViewModele() {
        return ACTOR_VIEW_MODELE;
    }

    public static ContentActorViewModel getContentActorViewModele() {
        return CONTENT_ACTOR_VIEW_MODELE;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // Lors du choix d'une options
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.vu) {
            Intent intent = new Intent(getApplicationContext(), VuActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.actors) {
            Intent intent = new Intent(getApplicationContext(), ActorActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Lors d'un click retour
    @Override
    public void onBackPressed() {
        long time = 2000;

        if (backPress + time > System.currentTimeMillis()) {
            this.backToast.cancel();
            super.onBackPressed();
        }
        else {
            this.backToast = Toast.makeText(getApplicationContext(), R.string.back_confirmation_home, Toast.LENGTH_SHORT);
            this.backToast.show();
        }
        this.backPress = System.currentTimeMillis();
    }
}
