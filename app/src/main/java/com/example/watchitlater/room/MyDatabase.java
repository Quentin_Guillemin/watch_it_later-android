package com.example.watchitlater.room;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.watchitlater.room.actor.Actor;
import com.example.watchitlater.room.actor.ActorDao;
import com.example.watchitlater.room.content.Content;
import com.example.watchitlater.room.content.ContentDao;
import com.example.watchitlater.room.content_actor.ContentActor;
import com.example.watchitlater.room.content_actor.ContentActorDao;

@Database(entities = {Content.class, Actor.class, ContentActor.class}, version = 3, exportSchema = false)
public abstract class MyDatabase extends RoomDatabase {

    private static MyDatabase INSTANCE;

    public abstract ContentDao contentDao();
    public abstract ActorDao actorDao();
    public abstract ContentActorDao contentActorDao();

    public static synchronized MyDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    MyDatabase.class, "watch_it_later")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }

        return INSTANCE;
    }

    // INSERTIONS AUTOMATIQUES A LA CREATION
    @SuppressWarnings("SpellCheckingInspection")
    private static final RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateBbAsyncTask(INSTANCE).execute();
        }
    };

    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private static class PopulateBbAsyncTask extends AsyncTask<Void, Void, Void> {

        private final ContentDao contentDao;
        private final ActorDao actorDao;

        private PopulateBbAsyncTask(MyDatabase database) {
            contentDao = database.contentDao();
            actorDao = database.actorDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // INSERTIONS
            return null;
        }
    }
}
