package com.example.watchitlater.room.content;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@SuppressWarnings("unused")
@Dao
public interface ContentDao {

    @Insert
    void insert(Content content);

    @Update
    void update(Content content);

    @Delete
    void delete(Content content);

    @Query("SELECT * FROM content WHERE id = :id")
    LiveData<Content> getContent(int id);

    @Query("SELECT id FROM content where title = :title")
    int getContentId(String title);

    @Query("SELECT COUNT(*) FROM content WHERE title = :title")
    int existContent(String title);

    @Query("SELECT * FROM content ORDER BY type, title ASC")
    LiveData<List<Content>> getAllContents();

    @Query("SELECT * FROM content WHERE vue = 1 ORDER BY type, title ASC")
    LiveData<List<Content>> getContentsVue();

    @Query("SELECT * FROM content WHERE vue = 0 ORDER BY type, title ASC")
    LiveData<List<Content>> getContentsNonVue();
}
