package com.example.watchitlater.room.content_actor;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

import com.example.watchitlater.room.actor.Actor;
import com.example.watchitlater.room.content.Content;

@Entity(
        indices = {@Index("contentId"), @Index("actorId")},
        tableName = "content_actor",
        primaryKeys = {"contentId", "actorId"},
        foreignKeys = {
                @ForeignKey(
                        entity = Content.class,
                        parentColumns = "id",
                        childColumns = "contentId"
                ),
                @ForeignKey(
                        entity = Actor.class,
                        parentColumns = "id",
                        childColumns = "actorId"
                )
        })
public class ContentActor {

    private int contentId;
    private int actorId;

    public ContentActor(int contentId, int actorId) {
        this.contentId = contentId;
        this.actorId = actorId;
    }

    @SuppressWarnings("unused")
    public int getContentId() {
        return contentId;
    }

    @SuppressWarnings("unused")
    public void setContentId(int contentId) {
        this.contentId = contentId;
    }

    @SuppressWarnings("unused")
    public int getActorId() {
        return actorId;
    }

    @SuppressWarnings("unused")
    public void setActorId(int actorId) {
        this.actorId = actorId;
    }
}
