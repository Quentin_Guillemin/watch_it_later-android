package com.example.watchitlater.room.content_actor;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.watchitlater.room.actor.Actor;
import com.example.watchitlater.room.content.Content;

import java.util.List;

@Dao
public interface ContentActorDao {

    @Insert
    void insert(ContentActor contentActor);

    @Delete
    void delete(ContentActor contentActor);

    @Query("SELECT * FROM content_actor WHERE contentId = :id")
    List<ContentActor> getContentActorOfContent(int id);

    @Query("SELECT COUNT(*) FROM content_actor WHERE contentId = :contentID AND actorId = :actorID")
    int existContentActor(int contentID, int actorID);

    @Query("SELECT content.* FROM content INNER JOIN content_actor ON content.id = content_actor.contentId WHERE actorId = :id ORDER BY type, title")
    LiveData<List<Content>> getAllContentsOfActor(int id);

    @Query("SELECT COUNT(content.id) FROM content INNER JOIN content_actor ON content.id = content_actor.contentId WHERE actorId = :id")
    int getNbContentsOfActor(int id);

    @Query("SELECT actor.* FROM actor INNER JOIN content_actor ON actor.id = content_actor.actorId WHERE contentId = :id")
    LiveData<List<Actor>> getAllActorsOfContent(int id);
}
