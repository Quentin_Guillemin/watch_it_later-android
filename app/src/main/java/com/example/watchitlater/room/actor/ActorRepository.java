package com.example.watchitlater.room.actor;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.watchitlater.room.MyDatabase;

import java.util.List;

public class ActorRepository implements ActorDao {

    private final ActorDao actorDao;

    ActorRepository(Application application) {
        MyDatabase database = MyDatabase.getInstance(application);
        this.actorDao = database.actorDao();
    }

    @Override
    public void insert(Actor actor) {
        new InsertActorAsyncTask(actorDao).execute(actor);
    }

    private static class InsertActorAsyncTask extends AsyncTask<Actor, Void, Void> {

        private final ActorDao actorDao;

        InsertActorAsyncTask(ActorDao actorDao) {
            this.actorDao = actorDao;
        }

        @Override
        protected Void doInBackground(Actor... actors) {
            if (actorDao.existActor(actors[0].getName()) == 0) {
                actorDao.insert(actors[0]);
            }
            return null;
        }
    }

    @Override
    public void delete(Actor actor) {
        new DeleteActorAsyncTask(actorDao).execute(actor);
    }

    private static class DeleteActorAsyncTask extends AsyncTask<Actor, Void, Void> {

        private final ActorDao actorDao;

        DeleteActorAsyncTask(ActorDao actorDao) {
            this.actorDao = actorDao;
        }

        @Override
        protected Void doInBackground(Actor... actors) {
            actorDao.delete(actors[0]);
            return null;
        }
    }

    @Override
    public LiveData<Actor> getActor(int id) {
        return actorDao.getActor(id);
    }

    @Override
    public int getActorId(String name) {
        return actorDao.getActorId(name);
    }

    @Override
    public int existActor(String name) {
        return actorDao.existActor(name);
    }

    @Override
    public LiveData<List<Actor>> getAllActors() {
        return actorDao.getAllActors();
    }
}
