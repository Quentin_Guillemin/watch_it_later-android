package com.example.watchitlater.room.actor;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "actor")
public class Actor {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private final String name;

    // Constructeur
    public Actor(String name) {
        this.name = name;
    }

    // Méthodes
    public int getId() {
        return id;
    }

    @SuppressWarnings("unused")
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
}
