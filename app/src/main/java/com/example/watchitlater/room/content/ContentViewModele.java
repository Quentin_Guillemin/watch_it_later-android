package com.example.watchitlater.room.content;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class ContentViewModele extends AndroidViewModel implements ContentDao {

    private final ContentRepository repository;

    public ContentViewModele(@NonNull Application application) {
        super(application);
        repository = new ContentRepository(application);
    }

    @Override
    public void insert(Content content) {
        repository.insert(content);
    }

    @Override
    public void update(Content content) {
        repository.update(content);
    }

    @Override
    public void delete(Content content) {
        repository.delete(content);
    }

    @Override
    public LiveData<Content> getContent(int id) {
        return repository.getContent(id);
    }

    @SuppressWarnings("unused")
    @Override
    public int existContent(String title) {
        return repository.existContent(title);
    }

    @Override
    public int getContentId(String title) {
        return repository.getContentId(title);
    }

    @SuppressWarnings("unused")
    @Override
    public LiveData<List<Content>> getAllContents() {
        return repository.getAllContents();
    }

    @Override
    public LiveData<List<Content>> getContentsVue() {
        return repository.getContentsVue();
    }

    @Override
    public LiveData<List<Content>> getContentsNonVue() {
        return repository.getContentsNonVue();
    }
}
