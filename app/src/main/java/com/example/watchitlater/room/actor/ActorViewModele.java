package com.example.watchitlater.room.actor;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class ActorViewModele extends AndroidViewModel implements ActorDao {

    private final ActorRepository repository;

    public ActorViewModele(@NonNull Application application) {
        super(application);
        repository = new ActorRepository(application);
    }

    @Override
    public void insert(Actor actor) {
        repository.insert(actor);
    }

    @Override
    public void delete(Actor actor) {
        repository.delete(actor);
    }

    @Override
    public LiveData<Actor> getActor(int id) {
        return repository.getActor(id);
    }

    @Override
    public int getActorId(String name) {
        return repository.getActorId(name);
    }

    @Override
    public int existActor(String name) {
        return repository.existActor(name);
    }

    @Override
    public LiveData<List<Actor>> getAllActors() {
        return repository.getAllActors();
    }
}
