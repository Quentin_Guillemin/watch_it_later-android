package com.example.watchitlater.room.content;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "content")
public class Content {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private final String title;
    private final String released;
    private final String runtime;
    private final String plot;
    private final String awards;
    private final String poster;
    private final String type;
    private String totalSeasons;
    private Boolean vue;

    // Constructeur
    public Content(String title, String released, String runtime, String plot, String awards, String poster, String type) {
        this.title = title;
        this.released = released;
        this.runtime = runtime;
        this.plot = plot;
        this.awards = awards;
        this.poster = poster;
        this.type = type;
        this.totalSeasons = null;
        this.vue = false;
    }

    // Méthodes
    public int getId() {
        return id;
    }

    @SuppressWarnings("unused")
    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public String getReleased() {
        return released;
    }

    public String getRuntime() {
        return runtime;
    }

    public String getPlot() {
        return plot;
    }

    public String getAwards() {
        return awards;
    }

    public String getPoster() {
        return poster;
    }

    @SuppressWarnings("unused")
    public String getType() {
        return type;
    }

    public String getTotalSeasons() {
        return totalSeasons;
    }

    public void setTotalSeasons(String totalSeasons) {
        this.totalSeasons = totalSeasons;
    }

    public Boolean getVue() {
        return vue;
    }

    public void setVue(Boolean vue) {
        this.vue = vue;
    }
}
