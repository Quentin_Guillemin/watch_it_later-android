package com.example.watchitlater.room.content;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.watchitlater.room.MyDatabase;

import java.util.List;

public class ContentRepository implements ContentDao {

    private final ContentDao contentDao;

    ContentRepository(Application application) {
        MyDatabase database = MyDatabase.getInstance(application);
        this.contentDao = database.contentDao();
    }

    // INSERT
    @Override
    public void insert(Content content) {
        new InsertContentAsyncTask(contentDao).execute(content);
    }

    private static class InsertContentAsyncTask extends AsyncTask<Content, Void, Void> {

        private final ContentDao contentDao;

        InsertContentAsyncTask(ContentDao contentDao) {
            this.contentDao = contentDao;
        }

        @Override
        protected Void doInBackground(Content... contents) {
            if (contentDao.existContent(contents[0].getTitle()) == 0) {
                contentDao.insert(contents[0]);
            }
            return null;
        }
    }

    // UPDATE
    @Override
    public void update(Content content) {
        new UpdateContentAsyncTask(contentDao).execute(content);
    }

    private static class UpdateContentAsyncTask extends AsyncTask<Content, Void, Void> {

        private final ContentDao contentDao;

        UpdateContentAsyncTask(ContentDao contentDao) {
            this.contentDao = contentDao;
        }

        @Override
        protected Void doInBackground(Content... contents) {
            contentDao.update(contents[0]);
            return null;
        }
    }

    // DELETE
    @Override
    public void delete(Content content) {
        new DeleteContentAsyncTask(contentDao).execute(content);
    }

    private static class DeleteContentAsyncTask extends AsyncTask<Content, Void, Void> {

        private final ContentDao contentDao;

        DeleteContentAsyncTask(ContentDao contentDao) {
            this.contentDao = contentDao;
        }

        @Override
        protected Void doInBackground(Content... contents) {
            contentDao.delete(contents[0]);
            return null;
        }
    }

    @Override
    public LiveData<Content> getContent(int id) {
        return contentDao.getContent(id);
    }

    @Override
    public int getContentId(String title) {
        return contentDao.getContentId(title);
    }

    @Override
    public int existContent(String title) {
        return contentDao.existContent(title);
    }

    @Override
    public LiveData<List<Content>> getAllContents() {
        return contentDao.getAllContents();
    }

    @Override
    public LiveData<List<Content>> getContentsVue() {
        return contentDao.getContentsVue();
    }

    @Override
    public LiveData<List<Content>> getContentsNonVue() {
        return contentDao.getContentsNonVue();
    }
}
