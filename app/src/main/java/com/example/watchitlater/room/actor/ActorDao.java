package com.example.watchitlater.room.actor;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ActorDao {

    @Insert
    void insert(Actor actor);

    @Delete
    void delete(Actor actor);

    @Query("SELECT * FROM actor WHERE id = :id")
    LiveData<Actor> getActor(int id);

    @Query("SELECT id FROM actor where name = :name")
    int getActorId(String name);

    @Query("SELECT COUNT(*) FROM actor WHERE name = :name")
    int existActor(String name);

    @Query("SELECT * FROM actor")
    LiveData<List<Actor>> getAllActors();
}
