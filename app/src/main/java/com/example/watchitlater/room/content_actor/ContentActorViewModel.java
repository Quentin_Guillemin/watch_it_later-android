package com.example.watchitlater.room.content_actor;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.watchitlater.room.actor.Actor;
import com.example.watchitlater.room.content.Content;

import java.util.List;

public class ContentActorViewModel extends AndroidViewModel implements ContentActorDao {

    private final ContentActorRepository repository;

    public ContentActorViewModel(@NonNull Application application) {
        super(application);
        repository = new ContentActorRepository(application);
    }

    @Override
    public void insert(ContentActor contentActor) {
        repository.insert(contentActor);
    }

    @Override
    public void delete(ContentActor contentActor) {
        repository.delete(contentActor);
    }

    @Override
    public List<ContentActor> getContentActorOfContent(int id) {
        return repository.getContentActorOfContent(id);
    }

    @Override
    public int existContentActor(int contentID, int actorID) {
        return repository.existContentActor(contentID, actorID);
    }

    @Override
    public LiveData<List<Content>> getAllContentsOfActor(int id) {
        return repository.getAllContentsOfActor(id);
    }

    @Override
    public int getNbContentsOfActor(int id) {
        return repository.getNbContentsOfActor(id);
    }

    @Override
    public LiveData<List<Actor>> getAllActorsOfContent(int id) {
        return repository.getAllActorsOfContent(id);
    }
}
