package com.example.watchitlater.room.content_actor;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.watchitlater.room.MyDatabase;
import com.example.watchitlater.room.actor.Actor;
import com.example.watchitlater.room.content.Content;

import java.util.List;

class ContentActorRepository implements ContentActorDao {

    private final ContentActorDao contentActorDao;

    ContentActorRepository(Application application) {
        MyDatabase database = MyDatabase.getInstance(application);
        this.contentActorDao = database.contentActorDao();
    }

    @Override
    public void insert(ContentActor contentActor) {
        new InsertContentActorAsyncTask(contentActorDao).execute(contentActor);
    }

    private static class InsertContentActorAsyncTask extends AsyncTask<ContentActor, Void, Void> {

        private final ContentActorDao contentActorDao;

        InsertContentActorAsyncTask(ContentActorDao contentActorDao) {
            this.contentActorDao = contentActorDao;
        }

        @Override
        protected Void doInBackground(ContentActor... contentActors) {
            if (contentActorDao.existContentActor(contentActors[0].getContentId(), contentActors[0].getActorId()) == 0) {
                contentActorDao.insert(contentActors[0]);
            }
            return null;
        }
    }

    @Override
    public void delete(ContentActor contentActor) {
        new DeleteContentActorAsyncTask(contentActorDao).execute(contentActor);
    }

    private static class DeleteContentActorAsyncTask extends AsyncTask<ContentActor, Void, Void> {

        private final ContentActorDao contentActorDao;

        DeleteContentActorAsyncTask(ContentActorDao contentActorDao) {
            this.contentActorDao = contentActorDao;
        }

        @Override
        protected Void doInBackground(ContentActor... contentActors) {
            contentActorDao.delete(contentActors[0]);
            return null;
        }
    }

    @Override
    public List<ContentActor> getContentActorOfContent(int id) {
        return contentActorDao.getContentActorOfContent(id);
    }

    @Override
    public int existContentActor(int contentID, int actorID) {
        return contentActorDao.existContentActor(contentID, actorID);
    }

    @Override
    public LiveData<List<Content>> getAllContentsOfActor(int id) {
        return contentActorDao.getAllContentsOfActor(id);
    }

    @Override
    public int getNbContentsOfActor(int id) {
        return contentActorDao.getNbContentsOfActor(id);
    }

    @Override
    public LiveData<List<Actor>> getAllActorsOfContent(int id) {
        return contentActorDao.getAllActorsOfContent(id);
    }
}
