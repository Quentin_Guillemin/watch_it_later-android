package com.example.watchitlater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.watchitlater.room.content.Content;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ContentAdapter2 extends RecyclerView.Adapter<ContentAdapter2.ContentHolder> {

    private List<Content> contents = new ArrayList<>();
    private OnItemClickListener listener;

    @NonNull
    @Override
    public ContentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_item2, parent, false);
        return new ContentHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ContentHolder holder, int position) {
        Content currentContent = contents.get(position);

        // ImageView
        Picasso.get()
            .load(currentContent.getPoster())
            //.placeholder(R.drawable.user_placeholder)
            //.error(R.drawable.user_placeholder_error)
            .into(holder.poster);

        // TextView
        holder.title.setText(currentContent.getTitle());
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
        // a remplacer
        notifyDataSetChanged();
    }

    class ContentHolder extends RecyclerView.ViewHolder {

        private final ImageView poster;
        private final TextView title;

        ContentHolder(@NonNull View itemView) {
            super(itemView);

            // Layout elements
            poster = itemView.findViewById(R.id.poster);
            title = itemView.findViewById(R.id.title);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(contents.get(position));
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Content content);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
