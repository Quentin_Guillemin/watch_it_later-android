package com.example.watchitlater.retrofit;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ContentRetro {

    @SerializedName("Title")
    private String title;

    @SerializedName("Released")
    private String released;

    @SerializedName("Runtime")
    private String runtime;

    @SerializedName("Actors")
    private String actors;

    @SerializedName("Plot")
    private String plot;

    @SerializedName("Awards")
    private String awards;

    @SerializedName("Poster")
    private String poster;

    @SerializedName("Type")
    private String type;

    @SerializedName("totalSeasons")
    private String totalSeasons;

    public String getTitle() {
        return title;
    }

    public String getReleased() {
        return released;
    }

    public String getRuntime() {
        return runtime;
    }

    public String getActors() {
        return actors;
    }

    public String getPlot() {
        return plot;
    }

    public String getAwards() {
        return awards;
    }

    public String getPoster() {
        return poster;
    }

    public String getType() {
        return type;
    }

    public String getTotalSeasons() {
        return totalSeasons;
    }
}
