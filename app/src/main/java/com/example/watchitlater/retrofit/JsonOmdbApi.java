package com.example.watchitlater.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

@SuppressWarnings("SpellCheckingInspection")
public interface JsonOmdbApi {

    @GET("/")
    Call<ContentRetro> getContent(@Query("t") String t, @Query("apikey") String key);
}
