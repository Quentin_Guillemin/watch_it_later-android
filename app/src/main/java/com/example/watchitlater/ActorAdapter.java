package com.example.watchitlater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.watchitlater.room.actor.Actor;

import java.util.ArrayList;
import java.util.List;

public class ActorAdapter extends RecyclerView.Adapter<ActorAdapter.ActorHolder> {

    private List<Actor> actors = new ArrayList<>();
    private OnItemClickListener listener;

    @NonNull
    @Override
    public ActorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.actor_item, parent, false);
        return new ActorHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ActorHolder holder, int position) {
        Actor currentActor = actors.get(position);

        holder.name.setText(currentActor.getName());
    }

    @Override
    public int getItemCount() {
        return actors.size();
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
        // a remplacer
        notifyDataSetChanged();
    }

    class ActorHolder extends RecyclerView.ViewHolder {

        private final TextView name;

        ActorHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(actors.get(position));
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Actor actor);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
