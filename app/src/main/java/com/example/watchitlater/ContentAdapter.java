package com.example.watchitlater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.watchitlater.room.content.Content;
import com.example.watchitlater.room.content.ContentViewModele;
import com.example.watchitlater.ui.MainActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class ContentAdapter extends RecyclerView.Adapter<ContentAdapter.ContentHolder> {

    // Paramètres
    private List<Content> contents = new ArrayList<>();
    private OnItemClickListener listener;

    @NonNull
    @Override
    public ContentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_item, parent, false);
        return new ContentHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ContentHolder holder, int position) {
        Content currentContent = contents.get(position);

        // ImageView
        Picasso.get()
            .load(currentContent.getPoster())
            //.placeholder(R.drawable.user_placeholder)
            //.error(R.drawable.user_placeholder_error)
            .into(holder.poster);

        // TextView
        holder.title.setText(currentContent.getTitle());

        // ImageButton
        if (currentContent.getVue())
            holder.vu.setImageResource(R.drawable.ic_not_view_black_24dp);
        else
            holder.vu.setImageResource(R.drawable.ic_done_black_24dp);
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
        // a remplacer
        notifyDataSetChanged();
    }

    class ContentHolder extends RecyclerView.ViewHolder {

        // ViewModele
        private final ContentViewModele contentViewModele;

        private final ImageView poster;
        private final TextView title;
        private final ImageButton vu;

        ContentHolder(@NonNull View itemView) {
            super(itemView);

            // ViewModele
            contentViewModele = MainActivity.getContentViewModele();

            // Layout elements
            poster = itemView.findViewById(R.id.poster);
            title = itemView.findViewById(R.id.title);
            vu = itemView.findViewById(R.id.vu);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(contents.get(position));
                    }
                }
            });

            // vu status
            vu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        Content c = contents.get(position);
                        if (c.getVue()) {
                            c.setVue(false);
                            Toasty.info(v.getContext(), R.string.toast_info_non_vu).show();
                        }
                        else {
                            c.setVue(true);
                            Toasty.info(v.getContext(), R.string.toast_info_vu).show();
                        }
                        contentViewModele.update(c);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Content content);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
